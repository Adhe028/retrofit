 package com.adhe18090088.retrofitlatihan

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import retrofit2.Call
import javax.security.auth.callback.Callback

 class MainActivity : AppCompatActivity() {

    private val list = ArrayList<PostResponse>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        
        rvPost.setHasFixedSize(true)
        rvPost.layoutManager = LinearLayoutManager(this)

        RetrofitClient.instance.getPosts().enqueue(object: Callback<ArrayList<PostResponse>>{
            override fun onFailure(call: Call<ArrayList<PostResponse>>, t: Throwable){

            }

            override fun onResponse(
                call: Call<ArrayList<PostResponse>>,
                response: PostResponse<ArrayList<PostResponse>>
            ){
                val responseCode = response.code().toString()
                tvResponCode.text = responseCode
                response.body()?.let{list.addAll(it)}
                val adapter = PostAdapter(list)
                rvPost.adapter = adapter
            }
        })
    }
}


